/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {
    private Board board;
    private Player player1;
    private Player player2;
    public boolean A = false;
    
    public Game(){
        player1 = new Player('X');
        player2 = new Player('O');
    }
    
    public void play() {
        showWelcome();
        while(true){
            newGame();
            showBoard();
            while (!board.checkWin() && !board.checkDraw()) {
                showTurn();
                inputChar();
                showBoard();
            }
            showResult();
            if(playAgain() == false){
                break;
            }
        }
    }
    
    private void showWelcome(){
        System.out.println("Welcome to XO Game!!");
    }
    
    public void showBoard(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(board.getBoard()[i][j]+ " ");
            }
            System.out.println();
        }
    }

    private void newGame() {
        board = new Board(player1, player2);
    }
    
    private void inputChar(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Enter Your row col: ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        board.setRowCol(row, col);
    }
    
    private void showTurn(){
        System.out.println("Player "+ board.getCurrentPlayer() + " Turn");
    }

    private void showResult() {
        if (board.checkWin() == true) {
            System.out.println("Player " + board.getCurrentPlayer() + " wins!");
            if (board.getCurrentPlayer() == player1.getSymbol()) {
                player1.win();
                player2.lose();
            } else {
                player1.lose();
                player2.win();
            }
        } if (board.checkDraw() == true) {
            System.out.println("Draw!");
            player1.draw();
            player2.draw();
        }
    }

    private boolean playAgain(){
        while(true){
            Scanner kb = new Scanner(System.in);
            System.out.print("Continue? (y/n): ");
            char Ans = kb.next().charAt(0);
            if (Ans == 'y' || Ans == 'Y') {
                A = true;
                break;
            } else if (Ans == 'n' || Ans == 'N') {
                A = false;
                break;
            } else {
                System.out.println("Please Try Agian");
            } 
        }
        return A;
    }
}
